import Component from '@ember/component';

/**
 * Function for checking the winning patterns by inserting the current 
 * state to check against
 */
function check_game_winner(state) {
    var patterns = [

        /*Grid for Reference
        *
        *[0,0][1,0][2,0][3,0][4,0][5,0][6,0]
        *[0,1][1,1][2,1][3,1][4,1][5,1][6,1]
        *[0,2][1,2][2,2][3,2][4,2][5,2][6,2]
        *[0,3][1,3][2,3][3,3][4,3][5,3][6,3]
        *[0,4][1,4][2,4][3,4][4,4][5,4][6,4]
        *[0,5][1,5][2,5][3,5][4,5][5,5][6,5]
        */

        /* 
        * Using the refernce grid above, the following patterns
        * have been worked out in order to match a win or a draw.
        */

        //Horizontal Pattern Checking
        [[0,0], [1,0], [2,0], [3,0]],
        [[1,0], [2,0], [3,0], [4,0]],
        [[2,0], [3,0], [4,0], [5,0]],
        [[3,0], [4,0], [5,0], [6,0]],
        [[0,1], [1,1], [2,1], [3,1]],
        [[1,1], [2,1], [3,1], [4,1]],
        [[2,1], [3,1], [4,1], [5,1]],
        [[3,1], [4,1], [5,1], [6,1]],
        [[0,2], [1,2], [2,2], [3,2]],
        [[1,2], [2,2], [3,2], [4,2]],
        [[2,2], [3,2], [4,2], [5,2]],
        [[3,2], [4,2], [5,2], [6,2]],
        [[0,3], [1,3], [2,3], [3,3]],
        [[1,3], [2,3], [3,3], [4,3]],
        [[2,3], [3,3], [4,3], [5,3]],
        [[3,3], [4,3], [5,3], [6,3]],
        [[0,4], [1,4], [2,4], [3,4]],
        [[1,4], [2,4], [3,4], [4,4]],
        [[2,4], [3,4], [4,4], [5,4]],
        [[3,4], [4,4], [5,4], [6,4]],
        [[0,5], [1,5], [2,5], [3,5]],
        [[1,5], [2,5], [3,5], [4,5]],
        [[2,5], [3,5], [4,5], [5,5]],
        [[3,5], [4,5], [5,5], [6,5]],

        //Vertical Pattern Checking
        [[0,0], [0,1], [0,2], [0,3]],
        [[0,1], [0,2], [0,3], [0,4]],
        [[0,2], [0,3], [0,4], [0,5]],
        [[1,0], [1,1], [1,2], [1,3]],
        [[1,1], [1,2], [1,3], [1,4]],
        [[1,2], [1,3], [1,4], [1,5]],
        [[2,0], [2,1], [2,2], [2,3]],
        [[2,1], [2,2], [2,3], [2,4]],
        [[2,2], [2,3], [2,4], [2,5]],
        [[3,0], [3,1], [3,2], [3,3]],
        [[3,1], [3,2], [3,3], [3,4]],
        [[3,2], [3,3], [3,4], [3,5]],
        [[4,0], [4,1], [4,2], [4,3]],
        [[4,1], [4,2], [4,3], [4,4]],
        [[4,2], [4,3], [4,4], [4,5]],
        [[5,0], [5,1], [5,2], [5,3]],
        [[5,1], [5,2], [5,3], [5,4]],
        [[5,2], [5,3], [5,4], [5,5]],
        [[6,0], [6,1], [6,2], [6,3]],
        [[6,1], [6,2], [6,3], [6,4]],
        [[6,2], [6,3], [6,4], [6,5]],
        
        //Diagonal Pattern Checking
        [[0,0], [1,1], [2,2], [3,3]],
        [[1,1], [2,2], [3,3], [4,4]],
        [[2,2], [3,3], [4,4], [5,5]],
        [[0,1], [1,2], [2,3], [3,4]],
        [[1,2], [2,3], [3,4], [4,5]],
        [[0,2], [1,3], [2,4], [3,5]],
        [[0,3], [1,2], [2,1], [3,0]],
        [[0,4], [1,3], [2,2], [3,1]],
        [[1,4], [2,3], [3,2], [4,1]],
        [[1,3], [2,2], [3,1], [4,0]],
        [[0,5], [1,4], [2,3], [3,2]],
        [[1,4], [2,3], [3,2], [4,1]],
        [[2,3], [3,2], [4,1], [5,0]],
        [[1,5], [2,4], [3,3], [4,2]],
        [[2,4], [3,3], [4,2], [5,1]],
        [[3,3], [4,2], [5,1], [6,0]],
        [[2,5], [3,4], [4,3], [5,2]],
        [[3,4], [4,3], [5,2], [6,1]],
        [[3,5], [4,4], [5,3], [6,2]],
        [[3,0], [4,1], [5,2], [6,3]],
        [[2,0], [3,1], [4,2], [5,3]],
        [[3,1], [4,2], [5,3], [6,4]],
        [[1,0], [2,1], [3,2], [4,3]],
        [[2,1], [3,2], [4,3], [5,4]],
        [[3,2], [4,3], [5,4], [6,5]],
    ];

    for(var pidx = 0; pidx < patterns.length; pidx++) {
        var pattern = patterns[pidx];
        var winner = state[pattern[0][0]][pattern[0][1]];
        if(winner) {
            for(var idx =1; idx < pattern.length; idx++) {
                if(winner != state[pattern[idx][0]][pattern[idx][1]]) {
                    winner = undefined;
                    break;
                }
            }
            if(winner) {
                return winner;
            }
        }
    }
    var draw = true;
    for(var x = 0; x <=2; x++) {
        for(var y = 0; y <= 2; y++) {
            if(!state[x][y]) {
                return undefined;
            }
        }
    }
    return '';
}

/**
 * This patterns object holds arrarys of patterns that are good moves in a way that checks
 * if the player is the current player, then by checking the x,y co-ordinates, the first one
 * for example shows if the player is the current player, and they will try to get 3 
 * markers in a vertical line, to be in a good position
 * the next ones represet horizontal and diagonal win potentials, 
 * these are then passed to the heuristic function
 */
var patterns = [
    {
        pattern: [['p', 0, 1], ['p', 0, 1], ['p', 0, 1],['p']],
        score: 1000
    },
    {
        pattern: [['p', 1, 0], ['p', 1, 0], ['p', 1, 0],['p']],
        score: 1000
    },
    {
        pattern: [['p', 1, 1], ['p', 1, 1], ['p', 1, 1],['p']],
        score: 1000
    },
    {
        pattern: [['p', 1, -1], ['p', 1, -1], ['p', 1, -1],['p']],
        score: 1000
    },
    {
        pattern: [['p', -1, 1], ['p', -1, 1], ['p', -1, 1],['p']],
        score: 1000
    },
    /**
     * These smaller patterns with lower scores represent a next move is a win situation where
     * getting one more next to your current counters would result in a win
     */
    {
        pattern: [['p', 0, 1], ['p']],
        score: 50
    },
    {
        pattern: [['p', 1, 0], ['p']],
        score: 50
    },
    {
        pattern: [['p', 1, 1], ['p']],
        score: 50
    },
    {
        pattern: [['p', 1, -1], ['p']],
        score: 50
    },
];

/**
 * Function to match the patterns to the states taking in the current game state,
 * the patterns, the current player and the x, y co-ordinates
 */
function match_pattern_at(state, pattern, player, x, y) {
    if(x >= 0 && x < state.length) {
        if(y >= 0 && y < state[x].length) {
            var element = pattern[0];
            if(element[0] == 'p') {
                if(state[x][y] !== player) {
                    return false;
                }
            }else if(element[0] == ' ') {
                if(state[x][y] !== undefined) {
                    return false;
                }
            }
            if(pattern.length > 1) {
                return match_pattern_at(state, pattern.slice(1), player, x + element[1], y + element[2])
            } else {
                return true;
            }
        }
    }
    return false;
}

//Function to match the patterns
function match_pattern(state, pattern, player) {
    for(var idx1 = 0; idx1 < state.length; idx1++) {
        for(var idx2 = 0; idx2 < state[idx1].length; idx2++) {
            var matches = match_pattern_at(state, pattern, player, idx1, idx2);
            if(matches) {
                return true;
            }
        }
    }
    return false;
}

/**
 * Heurisitc takes in the state and matches it to the patterns object containing 'good' moves
 * if the player is green the score is set to be its original value + that patterns value, if 
 * it is blue, it is minus that patterns value, this helps keep the game balanced by
 * determining which player is in the best posistion
 */
function heuristic(state) {
    var score = 0;
    for(var idx = 0; idx < patterns.length; idx++) {
        if(match_pattern(state, patterns[idx].pattern, 'Green')) {
            score = score + patterns[idx].score; 
        }
        if(match_pattern(state, patterns[idx].pattern, 'Blue')) {
            score = score - patterns[idx].score;
        }
    }
    return score;
}

/**
 * Uses minimax to loop of the games current state to find empty squares
 * that can be played in, and passes potential moves to the deepClone function to store.
 * Using the heuristic method above, scores are generated to determine how good a move is 
 * the better the moves score is, the more chance it will be played
 */
function minimax(state, limit, player) {
    var moves = [];
    if(limit > 0) {
        for(var idx1 = 0; idx1 < 7; idx1++) {
            for(var idx2 = 0; idx2 < 6; idx2++) {
                if(state[idx1][idx2] === undefined) {
                    var move = {
                        x: idx1,
                        y: idx2,
                        state: deepClone(state),
                        score: 0
                    };
                    move.state[idx1][idx2] = player;
                    if(limit === 1 || check_game_winner(move.state) !== undefined) {
                        move.score = heuristic(move.state);
                    } else {
                        move.moves = minimax(move.state, limit - 1, player == 'Blue' ? 'Green' : 'Blue');
                        var score = undefined;
                        for(var idx3 = 0; idx3 < move.moves.length; idx3++) {
                            if(score === undefined) {
                                score = move.moves[idx3].score;
                            } else if (player === 'Blue') {
                                score = Math.max(score, move.moves[idx3].score);
                            } else if (player === 'Green') {
                                score = Math.min(score, move.moves[idx3].score);
                            }
                        }
                        move.score = score;
                    }
                    moves.push(move);
                }
            }
        }
    }
    return moves;
}

/**
 * Computer move function which to let the computer play a marker on 
 * the board
 */
function computer_move(state) {
    var moves = minimax(state, 2, 'Green');
    var max_score = undefined;
    var move = undefined;
    for(var idx = 0; idx < moves.length; idx++) {
        if(max_score === undefined || moves[idx].score > max_score) {
            max_score = moves[idx].score;
            move = {
                x: moves[idx].x,
                y: moves[idx].y
            }
        }
    }
    /**
     * Used for debugging 
     * console.log(state);
     */
    return move;
}

/**
 * Clones the state to hold all the possible moves, saves it to a varabile called 
 * new_state when the length of the original state arrary is less than 1 and then
 * returns it
 */
function deepClone(state) {
    var new_state = [];
    for(var idx1 = 0; idx1 < state.length; idx1++) {
        new_state.push(state[idx1].slice(0));
    }
    return new_state;
}

export default Component.extend({
    playing: false,
    winner: undefined,
    draw: false,


    //Add the sound assets into the game
    init: function() {
        this._super(...arguments);
        createjs.Sound.registerSound("assets/sounds/place.ogg", "place-counter");
        createjs.Sound.registerSound("assets/sounds/start.ogg", "game-start");
        createjs.Sound.registerSound("assets/sounds/win.wav", "winner");
    },

    didInsertElement: function() {
        var stage = new createjs.Stage(this.$('#stage')[0]);

        //Draw the game board
        var board = new createjs.Shape();
        var graphics = board.graphics;
        graphics.beginFill('#a50000');
        //Border top
        graphics.drawRect(0, 0, 280, 3);
        //Horizontal Grid Lines
        graphics.drawRect(0, 40, 280, 2);
        graphics.drawRect(0, 80, 280, 2);
        graphics.drawRect(0, 120, 280, 2);
        graphics.drawRect(0, 160, 280, 2);
        graphics.drawRect(0, 200, 280, 2);
        graphics.drawRect(0, 240, 283, 3);
        //Left Border
        graphics.drawRect(0, 0, 3, 240);
        //Vertical Gridlines
        graphics.drawRect(40, 0, 2, 240);
        graphics.drawRect(80, 0, 2, 240);
        graphics.drawRect(120, 0, 2, 240);
        graphics.drawRect(160, 0, 2, 240);
        graphics.drawRect(200, 0, 2, 240);
        graphics.drawRect(240, 0, 2, 240);
        graphics.drawRect(280, 0, 3, 240);
        board.x = 40;
        board.y = 40;
        board.alpha = 0;
        this.set('board',board);
        stage.addChild(board);
        
        //Create the player markers/counters
        var markers = {
            'Blue': [],
            'Green': []
        }
        //21 because 42 sqaures totaled half means each square can have a counter
        for(var blue = 0; blue < 21; blue++) {
            var greenMarker = new createjs.Shape();
            graphics = greenMarker.graphics;
            graphics.beginFill('#00ff1e');
            graphics.drawCircle(0, 0, 15);
            graphics.endFill();
            greenMarker.visible = false;
            stage.addChild(greenMarker);
            markers.Green.push(greenMarker);
            
            //Draw the blue marker
            var blueMarker = new createjs.Shape();
            graphics = blueMarker.graphics;
            graphics.beginFill('#0080ff')
            graphics.drawCircle(0, 0, 15);
            graphics.endFill();
            blueMarker.visible = false;
            stage.addChild(blueMarker);
            markers.Blue.push(blueMarker);
        }
        this.set('markers', markers);
        this.set('stage', stage);
        //Call stage.update() at every tick
        createjs.Ticker.addEventListener("tick", stage);
    },
    
    //Click function
    click: function (ev) {
        var component = this;
        if (component.get("playing") && !component.get("winner")) {
            if(
                /**
                 *  If the target (User click) is on the canvas and within the area of 
                 * 40 x and y and 360 x and 280 y (the board area) then the click is registered
                 */  
                ev.target.tagName.toLowerCase() === "canvas" && ev.originalEvent.offsetX >= 40 && ev.originalEvent.offsetY >= 40 && ev.originalEvent.offsetX < 360 && ev.originalEvent.offsetY < 280) {
                
                /**
                 * Var X and Y are the grid coordinates
                 */
                var x = Math.floor((ev.originalEvent.offsetX - 40) / 40);
                var y = Math.floor((ev.originalEvent.offsetY - 40) / 40);
                //Retreive the game state
                var state = component.get('state');
                if(!state[x][y]) {
                    createjs.Sound.play("place-counter");
                    var move_count = component.get("moves")['Blue'];
                    var marker = component.get("markers")['Blue'][move_count];
                    //Sets the marker to blue
                    state[x][y] = 'Blue';
                    //Make it visible
                    marker.visible = true;
                    //Adjust the x and y values to make it more centred
                    marker.x = 60 + x * 40;
                    marker.y = 60 + y * 40;
                    //Calls the check winner function to see if someone has won
                    component.check_winner();
                    //Adds 1 to the move count of the blue player
                    component.get('moves')['Blue'] = move_count + 1;

                    /**
                     * Function for the computer player marker
                     */
                    setTimeout(function() {
                        if(!component.get('winner') && !component.get('draw')) {
                            var move = computer_move(state);
                            move_count = component.get("moves")['Green'];
                            state[move.x][move.y] = 'Green';
                            marker = component.get("markers")['Green'][move_count];
                            marker.visible = true;
                            marker.x = 60 + move.x * 40;
                            marker.y = 60 + move.y * 40;
                            component.get('moves')['Green'] = move_count + 1;
                            component.get('stage').update();
                            component.check_winner();
                        }
                    }, 500);
                }
            }
        }
      },
    
    /**
     * Checking for winner function which sets the state to the current game state
     * calls the check_game_winner function to test the current state against a winning
     * pattern, if the winner type does not match undefined the game is set to a draw
     * if not it recives the winning player and sets the winner variable to that finishing
     * with playing a sound to give the user an audio queue
     */
    check_winner: function() {
        var state = this.get('state');
        var winner = check_game_winner(state);
        if(winner !== undefined) {
            if(winner === '') {
                this.set('draw', true);
            } else {
                this.set('winner', winner);
                createjs.Sound.play("winner");
            }
        }
    },

    /**
     * Start function is called when the start or restart button is pressed
     * it plays a sound using createjs and soundjs, then it retrieves the board and sets its 
     * alpha to 0 to make it invisible, then sets it to 1 with a delay of 1500 milliseconds
     */
    actions: {
        start: function() {
            createjs.Sound.play("game-start");
            var board = this.get('board');
            board.alpha = 0;
            createjs.Tween.get(board).to({alpha: 1}, 1500);
            //Sets playing to true
            this.set('playing', true);
            //Set winner to undefined as the game has just started
            this.set('winner', undefined);
            //Sets Draw to false
            this.set('draw', undefined);
            //Sets the 7x6 grid to undefined showing no player has a counter in any grid cell
            this.set('state', [
                [undefined, undefined, undefined, undefined, undefined, undefined],
                [undefined, undefined, undefined, undefined, undefined, undefined],
                [undefined, undefined, undefined, undefined, undefined, undefined],
                [undefined, undefined, undefined, undefined, undefined, undefined],
                [undefined, undefined, undefined, undefined, undefined, undefined],
                [undefined, undefined, undefined, undefined, undefined, undefined],
                [undefined, undefined, undefined, undefined, undefined, undefined]]);
            //Set the number of moves for each player to 0
            this.set('moves', {'Blue': 0, 'Green': 0});
            //Set the first player to blue
            this.set("player", "Blue");
            //Get the markers arrary
            var markers = this.get('markers');
            /**
             * Ensures the markers visibility is set to false to remove old markers
             * from the board
             */
            for(var idx = 0; idx < 21; idx++) {
               markers.Blue[idx].visible = false;
               markers.Green[idx].visible = false;
            }
        }
    }
});