import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  //Route to add the game template to the index page
  this.route('game', {path: '/'});
  //Route for the instructions page
  this.route('instruction');
});

export default Router;
